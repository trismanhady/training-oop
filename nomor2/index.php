    <?php

        require_once('animal.php');
        require_once('frog.php');
        require_once('ape.php');

        $frog = new Frog ("Buduk");
        echo "Name : $frog->name <br>" ;
        echo "Legs : $frog->legs <br>" ;
        echo "Cold Blooded : $frog->cold_blooded <br>" ;
        echo $frog->jump();
        echo "<br><br>";

        $ape = new Ape ("sungokong");
        echo "Name : $ape->name <br>" ;
        echo "Legs : $ape->legs <br>" ;
        echo "Cold Blooded : $ape->cold_blooded <br>" ;
        echo $ape->yell();
    ?>
